<!DOCTYPE html>
<html lang="vi">
@include('layouts.head')
<body>
<div id="wrap">
@include('layouts.header')
<div class="container">
    @yield('content')
</div>
@include('layouts.footer')
</div>
</body>
</html>