<!DOCTYPE html>
<html lang={{ app()->getLocale() }}>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hello World</title>
</head>
<body>
<h1>Hello world!</h1><br/>
<h2>Hôm nay là ngày: {{date("d-m-Y")}}</h2>
</body>
</html>