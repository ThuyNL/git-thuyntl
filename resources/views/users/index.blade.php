@extends('layouts.default')
@section('title page')
    <title>Danh sách người dùng</title>
@endsection
@section('header title')
    Danh Sách Người Dùng
@endsection
@section('content')
    <div class="col-lg-10 col-lg-offset-1">
        <div class="panel panel-primary ">
            <div class="panel-heading">Danh Sách Người Dùng
                <div class="pull-right" style="display: inline">
                    <button type="button" class="pull-right btn btn-primary btn-xs" data-toggle="modal" data-target="#search-modal">Tìm kiếm</button>
                    @can('admin')
                        <a class="pull-right btn btn-primary btn-xs"  style="padding-left: 10px" href="{{ route('users.create') }}">Thêm Mới</a>
                    @endcan
                    <a class="pull-right btn btn-primary btn-xs"  style="padding-left: 10px" href="{{ route('users.index') }}">Danh sách</a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-auto table-condensed full_width">
                    <thead class="panel-title">
                    <th class="text-center">Số Thứ Tự</th>
                    <th class="text-center">Lớp</th>
                    <th class="text-center">Địa Chỉ Email</th>
                    <th class="text-center">Tên</th>
                    <th class="text-center">Địa Chỉ</th>
                    <th class="text-center">Số Điện Thoại</th>
                    <th class="text-center">Vai trò</th>
                    <th class="text-center">Sửa</th>
                    <th class="text-center">Xóa</th>
                    </thead>
                    <tbody>

                    @foreach ($users as $index => $user)
                        <tr>
                            <td class="text-center">{{ $index + 1}}</td>
                            <td class="text-center">{{ $user->classroom->name}}</td>
                            <td class="text-center">{{ $user->mail_address }}</td>
                            <td class="text-center">{{ StringHelper::stringToUpper($user->name) }}</td>
                            <td class="text-center">{{ $user->address }}</td>
                            <td class="text-center">{{ $user->phone }}</td>
                            <td class="text-center">{{ \App\Model\User::$role[$user->role] }}</td>
                            <td class="text-center">
                                <a href="{{ route('users.edit',['id' => $user->id]) }}" class="btn btn-xs btn-success">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a></td>
                            <td class="text-center">
                                <a href="{{ route('users.destroy',['id' => $user->id]) }}" class="btn btn-xs btn-success">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-right">{{ $users->links() }}</div>
            </div>
        </div>
        <!-- Search Modal -->
        <div id="search-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background-color: cornflowerblue">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="color: snow">Tìm kiếm người dùng</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frm-search" method="GET" enctype="multipart/form-data" action="{{route('users.index')}}">
                            <div class="form-group">
                                <label for="classroom_id">Lớp</label>
                                <select multiple class="form-control" name="classroom_id">
                                    @foreach($classrooms as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="mail_address">Email</label>
                                <input type="text" class="form-control" name="mail_address" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter name">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" placeholder="Enter address">
                            </div>
                            <div class="form-group">
                                <label for="address">Phone</label>
                                <input type="text" class="form-control" name="phone" placeholder="Enter phone">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="pull-right btn btn-primary" value="Tìm kiếm">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="pull-right btn btn-primary btn-outline-primary" data-dismiss="modal">Quay lại</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- End search modal -->
    </div>
    @endsection