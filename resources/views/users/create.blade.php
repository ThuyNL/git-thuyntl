@extends('layouts.default')
@section('title page')
    <title>Thêm mới người dùng</title>
@endsection
@section('header title')
    Thêm mới Người Dùng
@endsection
@section('content')
    @include('flash::message')
    <div class="col-lg-8 col-lg-offset-1">
        <div class="panel panel-primary ">
            <div class="panel-heading">Thêm Mới Người Dùng
                <div class="pull-right" style="display: inline">
                <a  class="btn btn-primary btn-xs" href="{{ route('users.create') }}">Thêm Mới Người Dùng</a>
                    <a  class="btn btn-primary btn-xs" href="{{ route('users.index') }}">Danh Sách Người Dùng</a>
                </div>
            </div>
            <div class="panel-body">
                <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group @if($errors->has('classroom_id')) {{'alert alert-danger'}} @endif">
                        <lable for="classroom_id">Lớp</lable>
                        <select
                               required
                               name="classroom_id"
                               class="form-control"
                        >
                            <option></option>
                            @foreach($classrooms as $id => $name)
                            <option value= "{{ $id }}" >{{ $name }}</option>
                            @endforeach
                        </select>
                            {!! $errors->first('classroom_id', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('mail_address')) {{'alert alert-danger'}} @endif">
                        <lable for="mail_address">Email</lable>
                        <input type="text"
                               placeholder="Enter email"
                               required
                               name="mail_address"
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('mail_address', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('password')) {{'alert alert-danger'}} @endif">
                        <lable for="password">Mật khẩu</lable>
                        <input type="password"
                               placeholder="Enter password"
                               required
                               name="password"
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('password_confirmation')) {{'alert alert-danger'}} @endif">
                        <lable for="password_confirmation">Nhập lại mật khẩu</lable>
                        <input type="password"
                               placeholder="Enter password confirm"
                               required
                               name="password_confirmation"
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('name')) {{'alert alert-danger'}} @endif">
                        <lable for="name">Tên</lable>
                        <input placeholder="Enter name"
                               name="name"
                               required
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('address')) {{'alert alert-danger'}} @endif">
                        <lable for="address">Địa chỉ</lable>
                        <input placeholder="Enter address"
                               name="address"
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group @if($errors->has('phone')) {{'alert alert-danger'}} @endif">
                        <lable for="phone">Số điện thoại</lable>
                        <input placeholder="Enter phone"
                               name="phone"
                               required
                               spellcheck="false"
                               class="form-control"
                        >
                            {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <lable for="role">Vai trò</lable>
                        <select id="role" name="role">
                            @foreach(\App\Model\User::$role as $id => $role)
                                <option value="{{ $id }}">{{ $role }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btn-add" class="btn btn-primary" value="Thêm">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

