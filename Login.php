<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<?php
$user = array('email' => 'thuyntl@gmail.com', 'password' => 'thuyntl123');
$data = array();
$errors = array();

//check exist cookie
if (isset($_COOKIE['email']) && isset($_COOKIE['password'])){
    if ($_COOKIE['email'] == $user['email'] && $_COOKIE['password'] == md5($user['password'])){
        header('Location: loginSuccess.php');
    }
}

//xử lí validate form login
if (isset($_POST['btn-submit'])){
    $data['email'] = isset($_POST['txt-email']) ? $_POST['txt-email'] : '';
    $data['password'] = isset($_POST['txt-passwd']) ? $_POST['txt-passwd'] : '';

    if (empty($data['email'])){
        $errors['email'] = 'Bạn chưa nhập email';
    } elseif (strlen($data['email']) > 255){
        $errors['email'] = 'Số kí tự email không vượt quá 255';
    } elseif (!is_email(test_input($data['email']))){
        $errors['email'] = 'Email không đúng định dạng';
    }

    if (empty($data['password'])){
        $errors['password'] = 'Bạn chưa nhập password';
    } elseif (strlen($data['password']) < 6 || strlen($data['password']) > 50){
        $errors['password'] = 'Password phải từ 6 đến 50 ký tự';
    } elseif (!is_password(test_input($data['password']))){
        $errors['password'] = 'Password chỉ được chứa chữ hoa, chữ thường và các ký tự đặc biệt';
    }

    if (!$errors && $data['email'] == $user['email'] && $data['password'] == $user['password']){
        session_start();
        $_SESSION['email'] = $data['email'];
        $_SESSION['password'] = $data['password'];
        echo 'Đăng nhập thành công!';

        if (isset($_POST['ckb-remember'])){
            setcookie('email', $data['email'], time()+60*60*24*365);
            setcookie('password', md5($data['password']), time()+60*60*24*365);
        }

        header('Location: LoginSuccess.php');
    }else{
        echo 'Đăng nhập không thành công';
    }
}

/**
 * function reject spacing at header and last of string, all of lashes and special chars
 * @param string
 * @return string
 */
function test_input($str)
{
$str = trim($str);
$str = stripcslashes($str);
$str = htmlspecialchars($str);
return $str;
}

/**
 * fucntion check email format
 * @param string email
 * @return boolen
 */
function is_email($email)
{
    return (!filter_var($email, FILTER_VALIDATE_EMAIL))? FALSE : TRUE;
}

/**
 * fucntion check password format
 * @param  string password
 * @return bool
 */
function is_password($passwd)
{
    return (!preg_match("/^[a-zA-Z][0-9a-zA-Z_!$@#^&]{6,50}$/", $passwd))? FALSE : TRUE;
}

?>
<h2>Login</h2>
<form name="frm_login" method="POST" action="Login.php">
<div name="dv-login">
    <table style="width: 500px; border: 0px" cellpadding="3" cellspacing="0">
        <tr>
            <td>Email</td>
            <td>
                <input type="text" name="txt-email" id="email" value="<?php echo isset($data['email']) ? $data['email'] : ''; ?>"/>
                <p style="color: red"><?php echo isset($errors['email']) ? $errors['email'] : ''; ?></p>
            </td>
        </tr>
        <tr>
            <td>Password</td>
            <td>
                <input type="password" name="txt-passwd" id="passwd" value="<?php echo isset($data['password']) ? $data['password'] : ''; ?>"/>
                <p style="color: red"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></p>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="checkbox" name="ckb-remember" value="remember-login"/>Remember me?</td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="btn-submit" id="btn-login" value="Login"/></td>
        </tr>
    </table>
</div>
</form>

</body>
</html>