<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Success</title>
</head>
<body>

    <?php
    session_start();
    if (isset($_POST['sm-logout'])){
        if (isset($_SESSION['email']) && isset($_SESSION['password'])){
            unset($_SESSION['email']);
            unset($_SESSION['password']);
        }

        if (isset($_COOKIE['email']) && isset($_COOKIE['password'])){
            setcookie('email', '', time()-60*60*24*365);
            setcookie('password', '', time()-60*60*24*365);
        }
        header('Location: Login.php');
    }
    ?>

<form name="frm-loginSuccess" method="post" action="">
    <?php
    $email = '';
    $user = '';
    $sesion = false;

    if (isset($_SESSION['email']) && isset($_SESSION['password'])){
        $sesion = true;
        $email = $_SESSION['email'];
        $paswd = $_SESSION['password'];
        $html = '<h4>Xin chào '.$email.' .Chúc mừng bạn đã đăng nhập thành công</h4>';
        $html .= '<input type="submit" name="sm-logout" value="Log Out"/>';
        echo $html;
    }

    if (isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
        if ($sesion){
            return 0;
        }
        $email = $_COOKIE['email'];
        $paswd = $_COOKIE['password'];
        $html = '<h4>Xin chào '.$email.' .Chúc mừng bạn đã đăng nhập thành công</h4>';
        $html .= '<input type="submit" name="sm-logout" value="Log Out"/>';
        echo $html;

    }

    ?>
</form>
</body>
</html>
