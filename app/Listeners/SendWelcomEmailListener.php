<?php

namespace App\Listeners;

use App\Jobs\SendRegisterEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Events\CreatedUser;
use App\Mail\RegisterEmail;

class SendWelcomEmailListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreatedUser $event)
    {
       dispatch(new SendRegisterEmail($event->user));
    }
}
