<?php
namespace App\Facades;

class StringHelper
{
    public function stringToUpper($str)
    {
        return mb_strtoupper($str, 'UTF-8');
    }
}