<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Model\Classroom;
use Illuminate\Http\Request;
use App\Model\User;
use App\Jobs\SendRegisterEmail;
use StringHelper;

class UserController extends Controller
{
    private $user;
    private $classroom;

    public function __construct(User $user, Classroom $classroom)
    {
        $this->middleware(['auth', 'classable']);
        $this->user = $user;
        $this->classroom = $classroom;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $this->authorize('staff');

        $data = $request->all();
        $users = $this->user->getUsers($data);
        $classrooms = $this->classroom->getClassrooms();
        return view('users.index', compact(['users', 'classrooms']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin');

        $classrooms = $this->classroom->getClassrooms();
        return view('users.create', compact('classrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $newUser = $this->user->createUser($data);
        if ($newUser) {
            event(new \App\Events\CreatedUser($newUser));
            flash('Thêm mới người dùng thành công')->success();
        }
    }
}
