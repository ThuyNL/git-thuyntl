<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Console\ViewClearCommand;
use Illuminate\View\View;

class HelloWorldController extends Controller
{
    public function show()
    {
        return View('hello-world.show');
    }
}