<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'classroom_id' => 'required',
            'mail_address' => 'required|unique:users,mail_address|email|max:100',
            'password' => 'required|max:191',
            'password_confirmation' => 'required_with:password|same:password|max:191',
            'name' => 'required|max:191',
            'address' => 'max:191',
            'phone' => 'required|max:15|regex:/^[0-9]+$/'
        ];
    }
}
