<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;

    protected $table = 'classrooms';
    protected $fillable = ['name'];
    protected $perPage = 20;
    protected $dates = ['deleted_at'];

    /**
     * create relationship with user
     */
    public function users()
    {
        return $this->hasMany(\App\Model\User::class);
    }

    /**
     * get list classroom
     */
    public function getClassrooms()
    {
        return Classroom::pluck('name', 'id');
    }
}
