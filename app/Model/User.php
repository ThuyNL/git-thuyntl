<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    protected $table = 'users';
    protected $fillable = ['classroom_id', 'mail_address', 'password', 'name', 'address', 'phone', 'role'];
    protected $hidden = ['password', 'remember_token'];
    protected $perPage = 20;
    protected $dates = ['deleted_at'];

    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    public static $role = [
        self::ROLE_ADMIN => 'Quản trị viên',
        self::ROLE_STAFF => 'Nhân viên',
    ];

    /**
     * create relationship with classroom
     */
    public function classroom()
    {
        return $this->belongsTo(\App\Model\Classroom::class);
    }

    /**
     * scope a query to include class_id of user
     */
    public function scopeOfClassroomId($query, $classroomId)
    {
        return $query->where('classroom_id', $classroomId);
    }

    /**
     * scope a query to include mail_address of user
     */
    public function scopeOfMailAddress($query, $mailAddress)
    {
        return $query->where('mail_address', 'LIKE', '%' . $mailAddress . '%');
    }

    /**
     * scope a query to include name of user
     */
    public function scopeOfName($query, $name)
    {
        return $query->where('name', 'LIKE', '%' . $name . '%');
    }

    /**
     * scope a query to include address of user
     */
    public function scopeOfAddress($query, $address)
    {
        return $query->where('address', 'LIKE', '%' . $address . '%');
    }

    /**
     * scope a query to include phone of user
     */
    public function scopeOfPhone($query, $phone)
    {
        return $query->where('phone', $phone);
    }

    /**
     * @return fixed
     * get list users
     */
    public function getUsers($data = array())
    {
        $query = User::with('classroom')->orderBy('mail_address', 'DESC');

        if (isset($data['classroom_id']) && $data['classroom_id']) {
            $query->ofClassroomId($data['classroom_id']);
        }

        if (isset($data['mail_address']) && $data['mail_address']) {
            $query->ofMailAddress($data['mail_address']);
        }

        if (isset($data['name']) && $data['name']) {
            $query->ofName($data['name']);
        }

        if (isset($data['address']) && $data['address']) {
            $query->ofAddress($data['address']);
        }

        if (isset($data['phone']) && $data['phone']) {
            $query->ofPhone($data['phone']);
        }

        return $query->paginate();

    }

    /**
     * @param array
     * @return $this|Model
     * insert new user. Input is data attributes user array
     */
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }
}

