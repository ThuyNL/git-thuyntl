<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\Classroom;

class ClassroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classrooms')->truncate();

        $faker = Faker\Factory::create('vi_VN');

        for ($i = 0; $i < 10; $i++){
            Classroom::create([
                'name' => $faker->name
            ]);
        }
    }
}
