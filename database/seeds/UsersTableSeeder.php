<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Model\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $faker = Faker\Factory::create('vi_VN');

        for ($i = 0; $i < 100; $i++){
            User::create([
                'classroom_id' => \App\Model\Classroom::all('id')->random()->id,
                'mail_address' => $faker->unique()->email,
                'password' => bcrypt($faker->password),
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber,
                'role' => rand(User::ROLE_ADMIN, User::ROLE_STAFF),

            ]);
        }
    }
}
